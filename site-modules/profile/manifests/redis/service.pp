class profile::redis::service {

service { 'redisService':
        name => 'redis-server',
        ensure => running,
}
}
