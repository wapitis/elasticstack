class profile::redis::install {

exec { 'apt-update':
	command => '/usr/bin/apt-get update',
	notify => Package['redisPackage'],
}

package { 'redisPackage':
        name => 'redis-server',
	ensure => present,
	provider => 'apt',
}

file { 'piddir':
        ensure => present,
        path => '/var/run/redis/redis-server.pid',
        owner => 'redis',
        group => 'redis',
        require => Package['redisPackage'],
}
}
