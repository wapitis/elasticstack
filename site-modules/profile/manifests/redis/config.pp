class profile::redis::config {

 $config_path = $profile::redis::initial::config_path
 
 file { 'redisConfig':
         ensure => present,
         path => '/etc/redis/redis.conf',
         content => epp($config_path),    # epp('profile/redis_config_yaml.epp'),
         owner => 'redis',
         group => 'redis',
         notify => Service['redisService'],
 }
}
