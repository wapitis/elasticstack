class profile::redis::initial {

  $config_path = lookup('redis::config_path')

  contain profile::redis::install
  contain profile::redis::config
  contain profile::redis::service

  
  Class['::profile::redis::install'] -> 
  Class['::profile::redis::config'] -> 
  Class['::profile::redis::service']
  
}
