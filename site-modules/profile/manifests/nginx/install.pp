class profile::nginx::install {

exec { 'apt-updateNginx':
	command => '/usr/bin/apt-get update',
	notify => Package['nginxPackage'],
}

package { 'nginxPackage':
        name => 'nginx',
	ensure => present,
	provider => 'apt',
}
}
