class profile::nginx::service {

service { 'nginxService':
        name => 'nginx',
        ensure => running,
}
}
