class profile::nginx::config {


 $config_path 	       = $profile::nginx::initial::config_path
 $bind_port 	       = $profile::nginx::initial::bind_port 
 $port_kibana 	       = $profile::nginx::initial::port_kibana   
 $nginx_username        = $profile::nginx::initial::nginx_username  
 $nginx_password        = $profile::nginx::initial::nginx_password 
 $kibana_user           = $profile::nginx::initial::kibana_user
 $nginx_userfile        = $profile::nginx::initial::nginx_userfile
 $nginx_config_location = $profile::nginx::initial::nginx_config_location
 $nginx_realm           = $profile::nginx::initial::realm
 
 
 
 $file_hash = {
        'bind_port'          => $bind_port,
        'port_kibana'        => $port_kibana,
 }
 
 
 httpauth { 'kibanaUser':
         username => $nginx_username,
         file     => $nginx_userfile,
         mode     => 0644,
         password => $nginx_password,
         mechanism => basic,
         realm => $nginx_realm,
         ensure => present,
         notify => File['htpasswdFile'],
 }
 
 file { 'htpasswdFile':
         ensure => present,
 	path => $nginx_userfile,
         mode => '555',
         notify => File['nginxConfig'],
 }
 
 file { 'nginxConfig':
         ensure  => present,
         path    => $nginx_config_location,
         content => epp($config_path, $file_hash),  
         owner   => $kibana_user,
         group   => $kibana_user,
         notify  => Service['nginxService'],
 }
}
