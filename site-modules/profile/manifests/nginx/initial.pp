class profile::nginx::initial ( 

String $config_path           = lookup('nginx::config_path'),
String $ip_kibana             = lookup('kibana::ip'),
String $bind_port             = lookup('nginx::bind_port'),
String $port_kibana           = lookup('kibana::port'),
Boolean $nginx_enabled        = lookup('nginx::enabled'),
String $nginx_username        = lookup('nginx::inputuser'),
String $nginx_password        = lookup('nginx::inputpass'),
String $kibana_user           = lookup('kibana::user'),
String $nginx_userfile        = lookup('nginx::userfile'),
String $nginx_config_location = lookup('nginx::config_location'),
String $nginx_realm           = lookup('nginx::realm'),

){
  contain profile::nginx::install
  contain profile::nginx::config
  contain profile::nginx::service

  
  Class['::profile::nginx::install'] -> 
  Class['::profile::nginx::config'] -> 
  Class['::profile::nginx::service']
  
}
