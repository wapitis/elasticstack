class profile::filebeat::service {

if $facts['os']['family'] == 'Debian'{

  file { $profile::filebeat::initial::lin_filebeat_service_path:
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    content => template($profile::filebeat::initial::lin_filebeat_service_conf),
    notify  => Service['filebeat'],
  }

  service { $profile::filebeat::initial::lin_filebeat_service_name:
    ensure => running,
    enable => true,
  }
}

if $facts['os']['family'] == 'Windows'{
  service {$profile::filebeat::initial::winbeat_service_name:
    ensure => running,
    enable => true,
 }

}

}

