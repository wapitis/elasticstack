# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include profile::filebeat
class profile::filebeat::initial (
String  $ip_kibana                 = lookup('kibana::ip'),
String  $port_kibana               = lookup('kibana::port'),
String  $ip_redis                  = lookup('redis::ip'),
String  $port_redis                = lookup('redis::port'),
String  $ip_logstash               = lookup('logstash::ip'),
String  $port_logstash             = lookup('logstash::port'),
String  $repo_path                 = lookup('elastic::repo_path'),
String  $repo_content              = lookup('elastic::repo_content'),
String  $dependency                = lookup('filebeat::dependency'),
String  $package_name              = lookup('filebeat::package_name'),
String  $lin_provider              = lookup('filebeat::lin_provider'),
String  $win_package_name          = lookup('filebeat::winbeat_package_name'),
String  $win_provider              = lookup('filebeat::winbeat_provider'),
String  $lin_filebeat_conf         = lookup('filebeat::lin_filebeat_conf'),
String  $lin_filebeat_conf_path    = lookup('filebeat::lin_filebeat_conf_path'),
String  $lin_filebeat_service_name = lookup('filebeat::package_name'),
String  $lin_filebeat_service_path = lookup('filebeat::lin_filebeat_service_path'),
String  $lin_filebeat_service_conf = lookup('filebeat::lin_filebeat_service_conf'),
Boolean $nginx_enabled             = lookup('nginx::enabled'),
String  $nginx_port                = lookup('nginx::bind_port'),
String  $input_user                = lookup('nginx::inputuser'),
String  $input_pass                = lookup('nginx::inputpass'),
String  $winbeat_conf              = lookup('filebeat::winbeat_conf'),
String  $winbeat_conf_path         = lookup('filebeat::winbeat_conf_path'),
String  $winbeat_service_name      = lookup('filebeat::winbeat_package_name'),
Boolean $redis_enabled             = lookup('redis::enabled'), 
String  $apt_key_id                = lookup('elastic::apt_key_id'),
String  $apt_key_source            = lookup('elastic::apt_key_source'),
){

contain ::profile::filebeat::install
contain ::profile::filebeat::config
contain ::profile::filebeat::service

Class['::profile::filebeat::install']->
Class['::profile::filebeat::config']~>
Class['::profile::filebeat::service']
}
