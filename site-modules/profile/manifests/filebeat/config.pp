class profile::filebeat::config {

  $ip_kibana              = $profile::filebeat::initial::ip_kibana
  $port_kibana            = $profile::filebeat::initial::port_kibana
  $ip_logstash            = $profile::filebeat::initial::ip_logstash
  $port_logstash          = $profile::filebeat::initial::port_logstash
  $ip_redis               = $profile::filebeat::initial::ip_redis
  $port_redis             = $profile::filebeat::initial::port_redis
  $file_name              = $profile::filebeat::initial::symlink
  $redis_enabled          = $profile::filebeat::initial::redis_enabled
  $nginx_enabled          = $profile::filebeat::initial::nginx_enabled
  $nginx_port             = $profile::filebeat::initial::nginx_port
  $input_user             = $profile::filebeat::initial::input_user
  $input_pass             = $profile::filebeat::initial::input_pass
  $lin_filebeat_conf_path = $profile::filebeat::initial::lin_filebeat_conf_path
  $lin_filebeat_conf      = $profile::filebeat::initial::lin_filebeat_conf
  $winbeat_conf_path      = $profile::filebeat::initial::winbeat_conf_path
  $winbeat_conf           = $profile::filebeat::initial::winbeat_conf

  $filebeat_hash = {
    'ip_kibana'     => $ip_kibana,
    'port_kibana'   => $port_kibana,
    'ip_logstash'   => $ip_logstash,
    'port_logstash' => $port_logstash,
    'ip_redis'      => $ip_redis,
    'port_redis'    => $port_redis,   
    'redis_enabled' => $redis_enabled,
    'nginx_enabled' => $nginx_enabled,
    'port_nginx'    => $nginx_port,   
    'input_user'    => $input_user,   
    'input_pass'    => $input_pass, 

   }

if $facts['os']['family'] == 'Debian'{
 
   file{ $lin_filebeat_conf_path :
    ensure  => present,
    owner   => 'root',
    content => epp($lin_filebeat_conf, $filebeat_hash),
   }
 }

 if $facts['os']['family'] == 'Windows' {

 file { $winbeat_conf_path:
   ensure  => present,
   content => epp($winbeat_conf, $filebeat_hash),
   notify  => Service['winlogbeat'],
  }
 }
}
