class profile::filebeat::install {

 $apt_key = $profile::filebeat::initial::apt_key_id
 $apt_key_source = $profile::filebeat::initial::apt_key_source
 $elastic_repo_path = $profile::filebeat::initial::repo_path
 $elastic_repo_content = $profile::filebeat::initial::repo_content
 $dependency =  $profile::filebeat::initial::dependency
 $filebeat_package_name = $profile::filebeat::initial::package_name
 $linux_package_provider = $profile::filebeat::initial::lin_provider
 $win_package_name = $profile::filebeat::initial::win_package_name
 $win_package_provider =  $profile::filebeat::initial::win_provider


# Using facter to get "os family name".
if $facts['os']['family'] == 'Debian' {
include apt

# In order to not get error on nodes where elasticsear and kibana is installed, due to apt-key and repo already added. 
unless ($facts['networking']['fqdn'] =~ /(search|kibana|stash).d?.node.consul/){
 
  apt::key { 'puppet gpg key':
      id     => $apt_key,
      source => $apt_key_source,
      notify  => Exec['apt-update-filebeat'],
  }

  file { 'filebeat_repo':
     ensure  => present,
     path    => $elastic_repo_path,
     content => $elastic_repo_content,

  }

  package{ 'dependency':
      name     => $dependency,
      ensure   => present,
      provider => $linux_package_provider,
   }

   exec {'apt-update-filebeat':
      command => '/usr/bin/apt-get update',
      require => [
        File['filebeat_repo'],
        Package['dependency'],
        ],
   }

   package { 'filebeat':
      name     => $filebeat_package_name,
      ensure   => present,
      provider => 'apt',
      require  => Exec['apt-update-filebeat'],
   }
}
if ($facts['networking']['fqdn'] =~ /(search|kibana|stash).d?.node.consul/){

   package {'filebeat':
      name     => $linux_package_name,
      ensure   => present,
      provider => 'apt', 
      require  => Exec['apt-update'],
   }
  }
}

if $facts['os']['family'] == 'Windows' {

 include chocolatey

  package {'winlogbeat':
      name     => $win_package_name,
      ensure   => present,
      provider => 'chocolatey',
   }
 }
}
