class profile::elasticsearch::initial(

 String $ip                = lookup('elasticsearch::ip'),
 String $port              = lookup('elasticsearch::port'),
 String $repo_path         = lookup('elastic::repo_path'),
 String $repo_content      = lookup('elastic::repo_content'),
 String $config_path       = lookup('elasticsearch::config_path'),
 String $config_template   = lookup('elasticsearch::config_template'),
 String $apt_key_id        = lookup('elastic::apt_key_id'),
 String $apt_key_source    = lookup('elastic::apt_key_source'),
 ) {
 
 contain ::profile::elasticsearch::install
 contain ::profile::elasticsearch::config
 contain ::profile::elasticsearch::service
 
 Class['::profile::elasticsearch::install']->
 Class['::profile::elasticsearch::config']~>
 Class['::profile::elasticsearch::service']
}
