class profile::elasticsearch::install { 

include ::apt

  $apt_key_id      = $profile::elasticsearch::initial::apt_key_id
  $apt_key_source  = $profile::elasticsearch::initial::apt_key_source
  $repo_path       = $profile::elasticsearch::initial::repo_path
  $repo_content    = $profile::elasticsearch::initial::repo_content
  $package_version = $profile::elasticsearch::initial::version

  # Add elasticsearch gpg key
  apt::key { 'puppet gpg key':
      id     => $apt_key_id,   
      source => $apt_key_source, 
      notify => Exec['apt-update'],
  }
  
  package { 'apt-transport-httpsPackage':
        name     => 'apt-transport-https',
  	ensure   => present,
  	provider => 'apt',
  }
  
  
  file { 'elasticRepo':
  	ensure  => present,
  	path    => $repo_path, 
  	content => $repo_content, 
  }
  
  exec { 'apt-update':
  	command => '/usr/bin/apt-get update',
  	require => [
  	  File['elasticRepo'],
  	  Package['apt-transport-httpsPackage'],
  	],
  }
  
  package { 'elasticPackage':
        name     => 'elasticsearch',
  	ensure   => $package_version,
  	provider => 'apt',
  	require  => Exec['apt-update'],
  }
  
  file { 'datadir':
        ensure  => directory,
        path    => '/data',
  	owner   => 'elasticsearch',
  	group   => 'elasticsearch',
  	recurse => true,
  	require => Package['elasticPackage'],
  }
  
  file { 'logdir':
        ensure  => directory,
        path    => '/logs',
  	owner   => 'elasticsearch',
  	group   => 'elasticsearch',
  	recurse => true,
  	require => Package['elasticPackage'],
  }
}
