class profile::elasticsearch::config {

  $port            = $profile::elasticsearch::initial::port
  $ip              = $profile::elasticsearch::initial::ip
  $config_path     = $profile::elasticsearch::initial::config_path
  $config_template = $profile::elasticsearch::initial::config_template
  
  $elastic_hash = { 
      'port'    => $port, 
      'ip'      => $ip, 
  }
  
  
  file { 'elasticconf':
          ensure  => present,
          path    => $config_path,  
          content => epp($config_template, $elastic_hash), 
          notify  => Service['elasticsearchService'],
  }
}

