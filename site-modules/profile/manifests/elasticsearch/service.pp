class profile::elasticsearch::service {

service { 'elasticsearchService':
        name => 'elasticsearch',
        ensure => running,
}
}
