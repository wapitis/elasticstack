class profile::logstash::install{
include ::apt
 
 $apt_key_id          = $profile::logstash::initial::apt_key_id
 $apt_key_source      = $profile::logstash::initial::apt_key_source
 $repo_path           = $profile::logstash::initial::repo_path
 $repo_content        = $profile::logstash::initial::repo_content
 $package_version     = $profile::elastic::initial::version
 
 exec { 'apt-update':
 	command => '/usr/bin/apt-get update',
 	notify => Package['java'],
 }
 
 # Add elasticsearch gpg key
 apt::key { 'puppet gpg key':
     id     => $apt_key_id,
     source => $apt_key_source,
     notify => Exec['apt-update2'],
 }
 
 package { 'java':
        name     => 'default-jre',
 	ensure   => present,
 	provider => 'apt',
 	notify   => File['logstash_repo'],
 }
 
 file { 'logstash_repo':
 	ensure  => present,
 	path    => $repo_path,
 	content => $repo_content,
 	notify  => Package['logstash_package'],
 }
 
 exec { 'apt-update2':
 	command => '/usr/bin/apt-get update',
 	require => File['logstash_repo'],
 }
 
 package { 'logstash_package':
        name => 'logstash',
 	ensure => $package_version,
 	provider => 'apt',
 	require => Exec['apt-update2'],
 }

}
