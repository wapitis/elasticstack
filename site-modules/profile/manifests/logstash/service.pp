class profile::logstash::service {

 service { 'logstash_service':
         name => 'logstash',
         ensure => running,
         require => Package['logstash_package'],
 }
}
