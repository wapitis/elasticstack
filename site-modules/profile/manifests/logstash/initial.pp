class profile::logstash::initial (                                                                                                     
String $ip_elasticsearch        = lookup('elasticsearch::ip'), 
String $port_logstash           = lookup('logstash::port'), 
String $ip_logstash             = lookup('logstash::ip'),
String $ip_redis                = lookup('redis::ip'),
Boolean $redis_enabled          = lookup('redis::enabled'), 

String $apt_key_id              = lookup('elastic::apt_key_id'),
String $apt_key_source          = lookup('elastic::apt_key_source'),
String $version                 = lookup('elastic::version'),

String $repo_path               = lookup('elastic::repo_path'),
String $repo_content            = lookup('elastic::repo_content'),
String $config_path             = lookup('logstash::config_path'),
String $config_log_path         = lookup('logstash::config_log_path'),

){
  contain ::profile::logstash::install
  contain ::profile::logstash::config
  contain ::profile::logstash::service

  Class['::profile::logstash::install']->
  Class['::profile::logstash::config']~>
  Class['::profile::logstash::service']
}

