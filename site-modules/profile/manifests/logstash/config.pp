class profile::logstash::config {

    $ip_elasticsearch	= $profile::logstash::initial::ip_elasticsearch
    $ip_logstash	= $profile::logstash::initial::ip_logstash
    $port_logstash	= $profile::logstash::initial::port_logstash
    $ip_redis		= $profile::logstash::initial::ip_redis
    $redis_enabled      = $profile::logstash::initial::redis_enabled
    $config_path        = $profile::logstash::initial::config_path
    $config_log_path    = $profile::logstash::initial::config_log_path
 
    $logstash_hash = {
      'ip_elasticsearch' => $ip_elasticsearch,
      'ip_logstash'      => $ip_logstash,
      'port_logstash'    => $port_logstash,
      'ip_redis'         => $ip_redis,
      'redis_enabled'    => $redis_enabled,
    }
    
    file { 'logconf':
    	ensure => present,
    	path => $config_log_path,
        content => epp($config_path,$logstash_hash),
        notify => Service['logstash_service'],
    }
}
