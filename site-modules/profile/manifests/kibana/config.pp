# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include kibana::config
class profile::kibana::config {
	
  $ip_elasticsearch   = $profile::kibana::initial::ip_elasticsearch
  $port_elasticsearch = $profile::kibana::initial::port_elasticsearch
  $ip_kibana          = $profile::kibana::initial::ip_kibana
  $kibana_localhost   = $profile::kibana::initial::kibana_localhost
  $port_kibana        = $profile::kibana::initial::port_kibana
  $nginx_enabled      = $profile::kibana::initial::nginx_enabled
  $config_path        = $profile::kibana::initial::config_path
  $config_template    = $profile::kibana::initial::config_template   
  $kibana_user        = $profile::kibana::initial::kibana_user 

  $file_hash = {
    'ip_elasticsearch'   => $ip_elasticsearch,
    'port_elasticsearch' => $port_elasticsearch,
    'ip_kibana'          => $ip_kibana,
    'port_kibana'        => $port_kibana,
    'kibana_localhost'   => $kibana_localhost,
    'nginx_enabled'      => $nginx_enabled,
  }
 
 
  file { 'kibanaconf':
    ensure  => present,
    path    => $config_path,
    content => epp($config_template, $file_hash),
    owner   => $kibana_user,
    group   => $kibana_user,
    mode    => '0660'
  }
}
