# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include kibana::install

class profile::kibana::install {
  
  include ::apt
  
  $apt_key_id       = $profile::kibana::initial::apt_key_id
  $apt_key_source   = $profile::kibana::initial::apt_key_source
  $repo_path        = $profile::kibana::initial::repo_path
  $repo_content     = $profile::kibana::initial::repo_content
  $package_version  = $profile::kibana::initial::version


  apt::key { 'puppet pgp key':
    id     => $apt_key_id,
    source => $apt_key_source,
    before => Exec['apt-update'],
  }


  file { 'kibanaRepo':
    ensure  => present,
    path    => $repo_path,
    content => $repo_content,
    before => Exec['apt-update'],
  }

  exec { 'apt-update':
    command => '/usr/bin/apt-get update',
    require => File['kibanaRepo'],
    notify => Package['kibanaPackage'],
  }

  package { 'kibanaPackage':
    name     => 'kibana',
    ensure   => '7.9.3', # Got error on version only for kibana with $package_version, was working before
    provider => 'apt',
  }
}
