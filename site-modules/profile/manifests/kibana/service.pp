class profile::kibana::service {
 
  service { 'kibanaService':
    name => 'kibana',
    ensure => running,
  }	
}
