# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include kibana
class profile::kibana::initial (
#Tuple
String $ip_elasticsearch   = lookup('elasticsearch::ip'),
String $port_elasticsearch = lookup('elasticsearch::port'),
String $ip_kibana          = lookup('kibana::ip'),
String $port_kibana        = lookup('kibana::port'),
String $kibana_localhost   = lookup('kibana::localhost'),
Boolean $nginx_enabled     = lookup('nginx::enabled'),

#install variables
String $apt_key_id         = lookup('elastic::apt_key_id'),
String $apt_key_source     = lookup('elastic::apt_key_source'),
String $version            = lookup('elastic::version'),

String $repo_path          = lookup('elastic::repo_path'),
String $repo_content       = lookup('elastic::repo_content'),

#config variables
String $config_path        = lookup('kibana::config_path'),
String $config_template    = lookup('kibana::config_template'),
String $nginx_bind_port    = lookup('nginx::bind_port'),
String $kibana_user        = lookup('kibana::user'),
){


  contain profile::kibana::install
  contain profile::kibana::config
  contain profile::kibana::service

  
  Class['::profile::kibana::install'] -> 
  Class['::profile::kibana::config'] -> 
  Class['::profile::kibana::service']
  
}
