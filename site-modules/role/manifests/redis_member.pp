class role::redis_member {
    include ::profile::base_linux
    include ::profile::dns::client
    include ::profile::consul::client
    include ::profile::redis::initial
    include ::profile::filebeat::initial
}
