class role::logstash_member {
    include ::profile::base_linux
    include ::profile::dns::client
    include ::profile::consul::client
    include ::profile::logstash::initial
    include ::profile::filebeat::initial

    Class['::profile::logstash::initial']->
    Class['::profile::filebeat::initial']
}
