class role::kibana_member {
    include ::profile::base_linux
    include ::profile::dns::client
    include ::profile::consul::client
    include ::profile::kibana::initial
    include ::profile::filebeat::initial
    include ::profile::nginx::initial

    Class['::profile::kibana::initial']->
    Class['::profile::nginx::initial']~>
    Class['::profile::filebeat::initial']
}
