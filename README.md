ELASTIC Stack
=========

### Table of Contents

1. [Overview](#overview)
2. [Module Description](#Module Description)
3. [Setup](#Setup)
4. [Future work](#Future work)

## Overview
This puppet module installs and configures the the different components of the [elastic stack](https://www.elastic.co/elastic-stack). 

## Module Description
Elasticstack is a composition of well known open source projects as kibana and elasticsearch. They are together with logstash the original ELK-stack but we have included beats, redis and nginx so this is an expanded elasticstack. Hiera is used to handle data and variables in this module. 

Currently is this module only tested on a heat-template linked to in  [setup](#Setup). This template creates several machines with name of the tool they are supposed to be used with.    

## Setup
We have made this to work with our environment that is spawned from this [heat-template](https://gitlab.com/wapitis/iac-heat-a). This template creates necessary security groups, a network and machines that the elastic stack can be installed on. 
It can probably be used on different infrastructures but then there need to be changes in addresses and naming-schemes. To get the setup working all you need to do is edit the common.yaml file located in the data folder and edit the variables suiting your needs.



## Future work

* Certificates
* Elasticsearch clusters
* Multiple Logstash nodes
* Working with Consul
